# Package specifications

## Version
`1.0.0`
Specification version number.

## Date

Last update date (iso): `2018-11-06T13:02:20.540Z`

## package

Package is a zip file containing at least :

* manifest file called: `mrmanisfest.json`
* a html file, referenced by mrmanisfest.json at `entry` property

Manifest file must follow the following specifications.


## manifest


### Name

mrmanifest.json

Name of manifest file.
This file must be present in the mrocs pack at the root.

Following properties describes structure of the `JSON` file.

#### Properties

* `schemaVersion` {Enum} **required** - Specification schema version. Available options:
  * 1.0.0

* `sdkVersion` {String} **required** - MROCS SDK version used. SDK ensure communication between b-eden and mrocs module (see package bdm-mrocs-sdk @todo)

* `entry` {String} **required** - Module entry point. Must be a relative path within the pack (eg: `"./index.html"`).
* `version` {String} **required** -  Package version (major.minor.patch), defaults to `1.0.0`
* `date` {String} **required** - Package generation date in ISO format - (eg: `(new Date).toISOString()`)
* `title` {String} **required** - Name of mrocs module.
* `language` {String} **required** - Language of the package ( ISO : fr|en|...)
* `resources` {String[]} **required** - Resource list in package. Must be relative path.

````json
  ...
  "resources": [
    "./mrmanifest.json",
    "./index.html",
    "./index.js",
    ...
  ],
  ...
````

* `author` {String} **optional** - Author name
* `uuid` {String} **optional** - Universal unique identifier
* `organization` {String} **optional** - Company name
* `description` {String} **optional** - Description of mrocs module.

* `repository` {Object} **optional** MROCS module repository. Available options:
  * `type` {String} **optional** - Should be a source control manager system type - eg: `git|svn|...`
  * `url` {String} **optional** - Should be a source control manager system repository link- eg: `"https://github.com/npm/cli.git"`

* `dependency` {Object} **optional** - Acquisition module dependency - For information or human check only.
  * `type` {String} **optinal** - Should be a source control manager system type (eg: `git|svn|...`) or reference of a system where is store the acquisition module (eg: `b-eden`)
  * `url` {String} **optional** - Should be a source control manager system repository link - eg: `"https://github.com/npm/cli.git"` or a link to the acquisition module
