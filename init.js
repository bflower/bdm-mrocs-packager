#!/usr/bin/env node
const path = require('path')
const inquirer = require('inquirer')
const utils = require(path.join(__dirname, 'lib', 'utils.js'))
const pkg = require('./lib/package')

const rPath = require('ramda/src/path')

const generateDefaultSettings = require(path.join(
  __dirname,
  'lib',
  'schemas',
  'config.init.js'
))

const validators = {
  notEmpty(value) {
    if (value && utils.cleanAndTrim(value) !== '') return true
    return 'Please enter a valid information'
  },
  number(value) {
    return !isNaN(parseFloat(value)) || 'Please enter a number'
  },
}

function getPkgKey(key, defValue = '') {
  const p = Array.isArray(key) ? key : key.split('.')
  const pkgJson = pkg.get()
  const v = rPath(p, pkgJson)
  return v === undefined ? defValue : v
}

const settings = {
  schemaVersion: '1.0.0',
  sdkVersion: '@todo',
  uuid: getPkgKey(`${pkg.KEY}.uuid`, utils.uuid()),
  language: 'fr', // ok
  organization: '', // ok
  title: `${getPkgKey('name')}-mr`, // ok
  source: 'dist_mrocs', // ok
  author: getPkgKey('author'),
  description: getPkgKey('description'),
  package: {
    entry: './index.html', // ok
    name: `${getPkgKey('name')}-mr`,
    outputFolder: './mrocs',
    zip: true,
  },
}

function trim(v) {
  return v.trim()
}
function trimLow(v) {
  return trim(v).toLowerCase()
}

function cliInit() {
  return inquirer
    .prompt([
      {
        type: 'input',
        name: 'title',
        message: 'Select title:',
        default: settings.title,
        validate: validators.notEmpty,
        filter: trim,
      },
      {
        type: 'input',
        name: 'organization',
        message: 'Select organization:',
        default: 'company_name',
        validate: validators.notEmpty,
        filter: trim,
      },
      {
        type: 'input',
        name: 'language',
        message: 'Select language:',
        default: settings.language,
        validate: validators.notEmpty,
        filter: trimLow,
      },
      {
        type: 'input',
        name: 'entry',
        message: 'Select starting page:',
        default: settings.package.entry,
        validate: validators.notEmpty,
        filter: trim,
      },
      {
        type: 'input',
        name: 'from',
        default: settings.source,
        filter: trim,
        validate: validators.notEmpty,
        message: 'Select source directory:',
      },
    ])
    .then(input => {
      settings.title = input.title
      settings.organization = input.organization
      settings.language = input.language
      settings.package.entry = input.entry
      settings.source = input.from || settings.source
    })
}


module.exports = function() {
  return cliInit()
    .then(() => {
      const pkgJson = pkg.get()
      const config = generateDefaultSettings(settings, pkgJson)

      pkgJson.mrocsPackager = config
      return pkg.save(pkgJson)
    })
    .then(() => {
      console.log('Saved to package.json')
    })
    .catch(e => {
      console.error('Error during bdm-mr-packager init param generator')
      console.error(e)
    })
}
