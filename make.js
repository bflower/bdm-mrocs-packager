const builder = require('./lib/build')
const pkg = require('./lib/package')

module.exports = make


function getPkgConf() {
  return pkg.get()[pkg.KEY]
}

function make() {
  return builder(getPkgConf())
}
