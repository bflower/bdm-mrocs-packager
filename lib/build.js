const fse = require('fs-extra')
const path = require('path')
const config = require('./schemas/config')
const pkg = require('./package')
const writeJsonFile = require('write-json-file')
const archiver = require('archiver')
const utils = require('./utils')
const glob = require('glob')

const MANIFEST_NAME = 'mrmanifest.json'

// getDirectorySize = require('./directory.js'),
// files = require('./files.js'),
// config = require('./schemas/config'),
// schema = require('./schema.js'),
// metadata = require('./metadata.js'),
// manifest = require('./manifest.js'),

function formatDate(isoDate) {
  const d = new Date(isoDate)
  return (
    d.getFullYear() +
    '-' +
    ('0' + (d.getMonth() + 1)).slice(-2) +
    '-' +
    ('0' + d.getDate()).slice(-2)
  )
}

function listResources(obj) {
  return new Promise((resolve, reject) => {
    glob(`**/*`, { nodir: true, cwd: path.join(process.cwd(), obj.source) }, (err, files) => {
      if (err) return reject(err)
      resolve(files)
    })
  })
  .then(files => {
    if (files.includes(MANIFEST_NAME) === false) {
      files.push(MANIFEST_NAME)
    }
    return files
  })
}

function _logSuccess(msg) {
  const date = new Date()
  const time =
    date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()
  console.log(`[${time}] MROCS '\x1b[32m${msg}\x1b[0m'`)
}

function _logError(err) {
  const date = new Date()
  const time =
    date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds()
  console.log(`[${time}] '\x1b[31m${err}\x1b[0m'`)
}

function ensureEntry(manifest, obj) {
  const entryPath = path.join(obj.source, obj.entry)
  return fse.pathExists(entryPath).then(exists => {
    if (!exists) throw new Error(`Entry path ${entryPath} does not exist !`)
    _logSuccess(`Entry path ${entryPath} exists !`)
    return true
  })
}

function writeManifest(manifest, obj) {
  const manifPath = path.join(obj.source, MANIFEST_NAME)
  return writeJsonFile(manifPath, manifest).then(() => {
    _logSuccess(`create ${manifPath}`)
  })
}

function makeFilename(manifest, obj) {
  const filename = [
    utils.cleanFilename(obj.package.name),
    `v${manifest.version}`,
  ]

  if (obj.package.incDay) {
    filename.push(formatDate(manifest.date))
  }

  return filename.join('_') + '.zip'
}

function archive(manifest, obj) {
  return new Promise((resolve, reject) => {
    if (!obj.package.zip) return resolve()
    fse.ensureDirSync(obj.package.outputFolder)

    const zipName = makeFilename(manifest, obj)
    const zipPath = path.join(obj.package.outputFolder, zipName)
    const output = fse.createWriteStream(zipPath)
    const archive = archiver('zip')

    _logSuccess(`Archiving ${obj.source} to ${zipPath}`)

    output.on('close', () => {
      _logSuccess(`${zipName} ${archive.pointer()} total bytes`)
      resolve('Done')
    })

    archive.on('error', err => {
      console.error(err)
      reject(err)
    })

    archive.pipe(output)
    archive.directory(obj.source, '')
    archive.finalize()
  })
}

function buildPackage(obj) {
  _logSuccess('Init')
  const manifest = config(obj, pkg.get())

  return ensureEntry(manifest, obj)
    .then(() => listResources(obj))
    .then(files => {
      manifest.resources = files
      return writeManifest(manifest, obj)
    })
    .then(() => archive(manifest, obj))
    .catch(e => {
      _logError(e)
    })
}

module.exports = buildPackage
