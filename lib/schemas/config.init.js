const utils = require('../utils')
const selfPkgJson = require('../../package.json')

module.exports = function(obj = {}, pkg) {
  if (!obj.package) obj.package = {}
  return {
    schemaVersion: selfPkgJson.specVersion,
    sdkVersion: '@todo',
    uuid: obj.uuid || utils.uuid(Date.now()),
    language: obj.language || 'fr',
    organization: obj.organization || '',
    title: obj.title || `${pkg.name}-mr`,
    source: obj.source || 'dist_mrocs',
    author: obj.author || pkg.author || '',
    description: obj.description || pkg.description || '',
    entry: obj.entry || './index.html',
    package: {
      name: obj.title || `${pkg.name}-mr` || '',
      outputFolder: obj.package.outputFolder || './mrocs',
      zip: obj.package.zip,
      incDay: true,
    },
    repository: obj.repository || pkg.repository,
    dependency: obj.dependency || pkg.repository,
  }
}
