const selfPkgJson = require('../../package.json')

module.exports = function(obj = {}, pkg) {
  if (!obj.package) obj.package = {}
  return {
    schemaVersion: selfPkgJson.specVersion,
    sdkVersion: '@todo',
    entry: obj.entry,
    version: pkg.version,
    date: (new Date).toISOString(),
    title: obj.title,
    description: obj.description,
    language: obj.language,
    author: obj.author,
    uuid: obj.uuid,
    organization: obj.organization,
    repository: obj.repository,
    dependency: obj.dependency,
    resources: [],
  }
}


