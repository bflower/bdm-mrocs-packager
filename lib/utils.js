function firstLetter(x) {
  return x ? x[0] : ''
}

module.exports = {
  cleanAndTrim(text) {
    //Escape all non alphanum characters
    return text.replace(/[^a-zA-Z\d\s]/g, '').replace(/\s/g, '')
  },
  cleanFilename(filename) {
    return filename.replace(/[^a-zA-Z\d\s-_().]/g, '')
  },
  acronym(words) {
    if (!words) {
      return ''
    }
    return words
      .split(' ')
      .map(firstLetter)
      .join('')
  },
  uuid(d = Date.now()) {
    const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      const r = (d + Math.random() * 16) % 16 | 0
      d = Math.floor(d / 16)
      return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16)
    })
    return uuid
  },
}
