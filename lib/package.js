const path = require('path')
const writePkg = require('write-pkg')

const KEY = 'mrocsPackager'

module.exports = {
  get,
  save,
  KEY,
  hasPackageConfig,
}

function hasPackageConfig() {
  return KEY in get()
}


function save(pkg) {
  return writePkg(process.cwd(), pkg)
}

function get() {
  try {
    const pkg = require(path.join(process.cwd(), 'package.json'))
    return pkg
  } catch (e) {
    return {}
  }
}
