# bdm-mrocs-packager

mrocs packager for b-eden MROCS modules (restitution part)

## Package specifications

see [specifications.md](./specifications.md)



## Installation

npm i @b-flower/bdm-mrocs-packager

## Commands

````
  init|i -> create param object for packager and store it in mrocsPackager key package.json
  make|m -> create mrocs archive based on config mrocsPackager in package.json
````

## Initialization
Initialization (cmd line) :

````
  $ npx bdm-mrocs-builder init
````

## Packager

Script in package.json

````json
  ...
  "pack": "bdm-mrocs-builder make"
  ...
````

Then in command line

````
  $ npm run pack
````

or `NPX` alternative

````
  $ npx bdm-mrocs-builder make
````